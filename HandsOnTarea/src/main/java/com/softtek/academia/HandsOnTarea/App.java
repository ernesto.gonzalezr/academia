package com.softtek.academia.HandsOnTarea;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
/**
 * Hello world!
 *
 */
public class App 
{
    public static  void main( String[] args )
    {
        List<Device> result = new ArrayList<com.softtek.academia.HandsOnTarea.Device>();
    	
    	dbConnection con=new dbConnection();
    	Connection conn=con.GetConection();
        PreparedStatement preparedStatement;
		try {
		
			String SQL_SELECT = "Select * from DEVICE";
			preparedStatement = conn.prepareStatement(SQL_SELECT);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				
				Device dev=new Device(resultSet.getInt("deviceId"),resultSet.getString("NAME"),resultSet.getString("description"),resultSet.getInt("manufacturerid"),resultSet.getInt("colorid"),resultSet.getString("comments"));
            	result.add(dev);
               System.out.println("ID: " + resultSet.getInt("deviceId") + " device name: " + resultSet.getString("NAME") + " desc value: " + resultSet.getString("description"));
            }
		} catch (SQLException e) {
			
			 System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		}catch (Exception e) {
			
			 System.err.format("SQL State:", e.getMessage());
		}

     
    	
    }
}
